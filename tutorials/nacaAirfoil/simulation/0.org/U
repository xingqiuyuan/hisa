/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    object      U;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

include         "include/freestreamConditions"

dimensions      [0 1 -1 0 0 0 0];

internalField   uniform $UInf;

boundaryField
{
    "(inlet_1|outlet_2)"
    {
        type            characteristicFarfieldVelocity;
        include         "include/freestreamConditions"
        value           $internalField;
    }

    symplane_3
    {
        type            empty;
    }

    wall_4
    {
        type            boundaryCorrectedFixedValue;
        value           uniform (0 0 0);
    }
}

// ************************************************************************* //
